#!/bin/bash

set -e

./test-cases/bin/iperf3 -c 10.5.1.222 -t 10 | tee tmp.txt
avg_speed=$(cat tmp.txt | grep 'receiver' | grep -oP '\d+\.\d+(?=\sMbits)')
THRESHOLD=90

echo "Average speed is $avg_speed Mbits/s"
echo "Threshold is $THRESHOLD Mbits/s"

pass=$(awk -v num1=$avg_speed -v num2=$THRESHOLD '
BEGIN {
    if (num1 > num2) {
        print 0;
    } else {
        print 1;
    }
}')

if [[ $pass -eq 0 ]]; then
	lava-test-case linux-posix-iperf3 --result pass
else
	lava-test-case linux-posix-iperf3 --result fail
fi